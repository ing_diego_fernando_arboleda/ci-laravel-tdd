<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Invalid user or password.',
    'exists' => 'Email exists. ',
    'disabled' => 'The user is disabled.',
    'notallowed' => 'Not allowed.',
    'missingtoken' => 'Token not provided.',
    'epiredtoken' => 'Token is expired.', 
    'registerok' => 'User has been created',
    'loginok' => 'User has been logged',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
