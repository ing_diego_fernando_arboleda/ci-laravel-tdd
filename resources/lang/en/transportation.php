<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Transportation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'busmessage' => 'Take the :name bus from :origin to :destination. ',
    'busseat' => 'Sit in seat :seat',
    'busnoseat' => 'No seat assignment',
];
