<?php

namespace Tests\Unit\Trip;

use PHPUnit\Framework\TestCase;
use App\Trip\TripSorter;
use App\Trip\Cards\Factories\CardFactory;
use App\Trip\Cards\Factories\CardFactoryInterface;
use App\Trip\Transportation\Factories\TransportFactory;
use App\Trip\Places\Factories\PlaceFactory;
use Api\Cards\AbstractCard;
use Api\Places\AbstractPlace;
use Prophecy\Argument;

class TripSorterTest extends TestCase
{
    public function testTripSorter(): void
    {
        $cardFactory = $this->getMockBuilder(CardFactoryInterface::class)
                                        ->getMock();
        $tripSorter = new TripSorter($cardFactory);
        $this->assertInstanceOf(TripSorter::class, $tripSorter);
    }

    public function tripSorterDataprovider(): array
    {
        $tripSorter = new TripSorter(
            new CardFactory(new TransportFactory(), new PlaceFactory())
        );

        return [
            'tripSorter' => [$tripSorter]
        ];
    }

    /** @dataProvider tripSorterDataprovider */
    public function testSort(TripSorter $tripSorter): void
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];

        $listCards = $tripSorter->sort($unsortCards);
        $this->assertIsArray($listCards);
    }

    /** @dataProvider tripSorterDataprovider */
    public function testSortSingle(TripSorter $tripSorter): void
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $listCards = $tripSorter->sort($unsortCards);
        $this->assertIsArray($listCards);
    }

    /** @dataProvider tripSorterDataprovider */
    public function testSortNext(TripSorter $tripSorter): void
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];
        $itinerary = $tripSorter->sort($unsortCards);
        $this->assertIsArray($itinerary);
    }
}
