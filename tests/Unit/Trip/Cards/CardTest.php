<?php

namespace Tests\Unit\Trip\Cards;

use PHPUnit\Framework\TestCase;
use App\Trip\Places\AbstractPlace;
use App\Trip\Transportation\AbstractTransport;
use App\Trip\Cards\Card;

class CardTest extends TestCase
{

    private const GATE = '45B';
    private const BAGGAGE_MESSAGE = 'Baggage will we automatically transferred from your last leg.';

    public function testCard(): Card
    {
        $card = new Card();
        $this->assertInstanceOf(Card::class, $card);
        return $card;
    }

    /** @depends testCard */
    public function testGetOrigin(Card $card): Card
    {
        $place = $this->getMockBuilder(AbstractPlace::class)->getMock();
        $card->setOrigin($place);
        $this->assertInstanceOf(AbstractPlace::class, $card->getOrigin());
        return $card;
    }

    /** @depends testGetOrigin */
    public function testGetDestination(Card $card): Card
    {
        $place = $this->getMockBuilder(AbstractPlace::class)->getMock();
        $card->setDestination($place);
        $this->assertInstanceOf(AbstractPlace::class, $card->getDestination());
        return $card;
    }

    /** @depends testGetDestination */
    public function testGetTransport(Card $card): Card
    {
        $transport = $this->getMockBuilder(AbstractTransport::class)->getMock();
        $card->setTransport($transport);
        $this->assertInstanceOf(AbstractTransport::class, $card->getTransport());
        return $card;
    }

    /** @depends testGetTransport */
    public function testGetGate(Card $card): Card
    {
        $card->setGate(self::GATE);
        $this->assertEquals(self::GATE, $card->getGate());
        return $card;
    }

    /** @depends testGetGate */
    public function testGetBaggageMessage(Card $card): Card
    {
        $card->setBaggageMessage(self::BAGGAGE_MESSAGE);
        $this->assertEquals(self::BAGGAGE_MESSAGE, $card->getBaggageMessage());
        return $card;
    }

    /** @depends testGetBaggageMessage */
    public function testGetItinerary(Card $card): void
    {
        $message = $card->getItinerary();
        $this->assertNotEmpty($message);
    }
}
