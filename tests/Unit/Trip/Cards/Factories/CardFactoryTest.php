<?php

namespace Tests\Unit\Trip\Cards\Factories;

use PHPUnit\Framework\TestCase;
use App\Trip\Cards\Factories\CardFactory;
use App\Trip\Places\Factories\PlaceFactory;
use App\Trip\Exceptions\Cards\Factories\InvalidCardException;
use App\Trip\Exceptions\ExceptionCodes;
use App\Trip\Places\AbstractPlace;
use App\Trip\Transportation\AbstractTransport;
use App\Trip\Transportation\Factories\TransportFactoryInterface;
use App\Trip\Transportation\Train;
use App\Trip\Transportation\Bus;
use App\Trip\Transportation\Flight;
use App\Trip\Cards\AbstractCard;
use Prophecy\Argument;

class CardFactoryTest extends TestCase
{

    private const GATE = '45B';
    private const BAGGAGE_MESSAGE = 'Baggage will we automatically transferred from your last leg.';

    private function getCardFactory(): CardFactory
    {
        $transportFactory = $this->getMockBuilder(TransportFactoryInterface::class)
                                        ->setMethods(['getTransport'])
                                        ->getMock();

        $placeFactory = $this->getMockBuilder(PlaceFactory::class)
                                        ->setMethods(['getPlace'])
                                        ->getMock();

        $factory = new CardFactory(
            $transportFactory,
            $placeFactory
        );
        return $factory;
    }

    public function cardFactoryDataprovider(): array
    {
        return [
            'cardFactory' => [$this->getCardFactory()]
        ];
    }

    public function testCardFactory(): void
    {
        $transportFactory = $this->getMockBuilder(TransportFactoryInterface::class)->getMock();
        $placeFactory = $this->getMockBuilder(PlaceFactory::class)->getMock();

        $factory = new CardFactory(
            $transportFactory,
            $placeFactory
        );
        $this->assertInstanceOf(CardFactory::class, $factory);
    }

    /** @dataProvider cardFactoryDataprovider */
    public function testGetCard(CardFactory $factory): void
    {
        $origin = $this->getMockBuilder(AbstractPlace::class)->getMock();
        $destination = $this->getMockBuilder(AbstractPlace::class)->getMock();
        $tranport = $this->getMockBuilder(AbstractTransport::class)->getMock();

        $card = $factory->getCard(
            self::GATE,
            self::BAGGAGE_MESSAGE,
            $origin,
            $destination,
            $tranport
        );

        $this->assertInstanceOf(AbstractCard::class, $card);
    }

    /** @dataProvider cardFactoryDataprovider */
    public function testBuildCardInvalidCardDataException(CardFactory $factory): void
    {
        $this->expectException(InvalidCardException::class);
        $this->expectExceptionCode(ExceptionCodes::CARD_INVALID_DATA);

        $data =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(AbstractCard::class, $card);
    }

    /** @dataProvider cardFactoryDataprovider */
    public function testBuildCardTrain(CardFactory $factory): void
    {
        $data =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(AbstractCard::class, $card);
    }

    /** @dataProvider cardFactoryDataprovider */
    public function testBuildCardBus(CardFactory $factory): void
    {
        $data =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(AbstractCard::class, $card);
    }

    /** @dataProvider cardFactoryDataprovider */
    public function testBuildCardFlight(CardFactory $factory): void
    {
        $data =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm.',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];
        $card = $factory->buildCard($data);
        $this->assertInstanceOf(AbstractCard::class, $card);
    }
}
