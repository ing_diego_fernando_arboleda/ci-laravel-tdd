<?php

namespace Tests\Unit\Trip\Transportation;

use PHPUnit\Framework\TestCase;
use App\Trip\Transportation\Bus;

class BusTest extends TestCase
{
    private const NAME = 'airport';
    private const SEAT = '21B';

    public function testBus(): Bus
    {
        $bus = new Bus();
        $this->assertInstanceOf(Bus::class, $bus);
        return $bus;
    }

    /** @depends testBus */
    public function testGetName(Bus $bus): Bus
    {
        $bus->setName(self::NAME);
        $this->assertEquals(self::NAME, $bus->getName());
        return $bus;
    }

    /** @depends  testGetName */
    public function testGetSeat(Bus $bus): Bus
    {
        $bus->setSeat(self::SEAT);
        $this->assertEquals(self::SEAT, $bus->getSeat());
        return $bus;
    }

    /** @depends testGetSeat */
    public function testGetMessage(Bus $bus): void
    {
        $message = $bus->getMessage();
        $this->assertStringStartsWith('Take the', $message);
    }

    /** @depends testGetSeat */
    public function testGetMessageNoSeat(Bus $bus): void
    {
        $bus->setSeat('');
        $message = $bus->getMessage();
        $this->assertStringStartsWith('Take the', $message);
    }
}
