<?php

namespace Tests\Unit\Trip\Transportation;

use PHPUnit\Framework\TestCase;
use App\Trip\Transportation\Train;

class TrainTest extends TestCase
{
    private const NAME = 'UH78';
    private const SEAT = '57B';

    public function testTrain(): Train
    {
        $train = new Train();
        $this->assertInstanceOf(Train::class, $train);
        return $train;
    }

    /** @depends testTrain */
    public function testGetName(Train $train): Train
    {
        $train->setName(self::NAME);
        $this->assertEquals(self::NAME, $train->getName());
        return $train;
    }

    /** @depends  testGetName */
    public function testGetSeat(Train $train): Train
    {
        $train->setSeat(self::SEAT);
        $this->assertEquals(self::SEAT, $train->getSeat());
        return $train;
    }

    /** @depends testGetSeat */
    public function testGetMessage(Train $train): void
    {
        $message = $train->getMessage();
        $this->assertStringStartsWith('Take train', $message);
    }

    /** @depends testGetSeat */
    public function testGetMessageNoSeat(Train $train): void
    {
        $train->setSeat('');
        $message = $train->getMessage();
        $this->assertStringStartsWith('Take train', $message);
    }
}
