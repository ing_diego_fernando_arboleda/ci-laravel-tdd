<?php

namespace Tests\Unit\Trip\Transportation\Factories;

use PHPUnit\Framework\TestCase;
use App\Trip\Transportation\Factories\TransportFactory;
use App\Trip\Exceptions\Transportation\Factories\InvalidTransportException;
use App\Trip\Exceptions\Transportation\Factories\MissingDataException;
use App\Trip\Transportation\AbstractTransport;
use App\Trip\Exceptions\ExceptionCodes;

class TransportFactoryTest extends TestCase
{
    public function trasportFactoryProvider(): array
    {
        return [
            'tranportFactory' => [new TransportFactory()]
        ];
    }

    public function testTransportFactory(): void
    {
        $transportFactory = new TransportFactory();
        $this->assertInstanceOf(TransportFactory::class, $transportFactory);
    }

    /** @dataProvider trasportFactoryProvider */
    public function testGetTransportMissingType(TransportFactory $transportFactory): void
    {
        $this->expectException(MissingDataException::class);
        $this->expectExceptionCode(ExceptionCodes::TRANSPORT_MISSING_DATA);
        $transportFactory->getTransport([]);
    }

    /** @dataProvider trasportFactoryProvider */
    public function testGetTransportInvalidType(TransportFactory $transportFactory): void
    {
        $this->expectException(InvalidTransportException::class);
        $this->expectExceptionCode(ExceptionCodes::TRANSPORT_INVALID_TYPE);

        $transportFactory->getTransport(
            ['type' => 'ufo', 'name' => 'one', 'seat' => '1']
        );
    }

    /** @dataProvider trasportFactoryProvider */
    public function testGetTransportMissingName(TransportFactory $transportFactory): void
    {
        $this->expectException(MissingDataException::class);
        $this->expectExceptionCode(ExceptionCodes::TRANSPORT_MISSING_DATA);
        $transportFactory->getTransport(['type' => 'bus']);
    }

    /** @dataProvider trasportFactoryProvider */
    public function testGetTransportBus(TransportFactory $transportFactory): void
    {
        $transport = $transportFactory->getTransport(
            ['type' => 'bus', 'name' => 'one', 'seat' => '1']
        );

        $this->assertInstanceOf(AbstractTransport::class, $transport);
    }

    /** @dataProvider trasportFactoryProvider */
    public function testGetTransportTrain(TransportFactory $transportFactory): void
    {
        $transport = $transportFactory->getTransport(
            ['type' => 'train', 'name' => 'two', 'seat' => '2']
        );

        $this->assertInstanceOf(AbstractTransport::class, $transport);
    }

    /** @dataProvider trasportFactoryProvider */
    public function testGetTransportFlight(TransportFactory $transportFactory): void
    {
        $transport = $transportFactory->getTransport(
            ['type' => 'flight', 'name' => 'three', 'seat' => '3']
        );

        $this->assertInstanceOf(AbstractTransport::class, $transport);
    }
}
