<?php

namespace Tests\Unit\Trip\Transportation;

use PHPUnit\Framework\TestCase;
use App\Trip\Transportation\Flight;

class FlightTest extends TestCase
{
    private const NAME = 'BC78';
    private const SEAT = 'A02';

    public function testFlight(): Flight
    {
        $flight = new Flight();
        $this->assertInstanceOf(Flight::class, $flight);
        return $flight;
    }

    /** @depends testFlight */
    public function testGetName(Flight $flight): Flight
    {
        $flight->setName(self::NAME);
        $this->assertEquals(self::NAME, $flight->getName());
        return $flight;
    }

    /** @depends  testGetName */
    public function testGetSeat(Flight $flight): Flight
    {
        $flight->setSeat(self::SEAT);
        $this->assertEquals(self::SEAT, $flight->getSeat());
        return $flight;
    }

    /** @depends testGetSeat */
    public function testGetMessage(Flight $flight): void
    {
        $message = $flight->getMessage();
        $this->assertStringStartsWith('From ', $message);
    }

    /** @depends testGetSeat */
    public function testGetMessageNoSeat(Flight $flight): void
    {
        $flight->setSeat('');
        $message = $flight->getMessage();
        $this->assertStringStartsWith('From ', $message);
    }
}
