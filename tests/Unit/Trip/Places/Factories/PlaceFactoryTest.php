<?php

namespace Tests\Unit\Trip\Places\Factories;

use PHPUnit\Framework\TestCase;
use App\Trip\Places\Factories\PlaceFactory;
use App\Trip\Exceptions\Places\Factories\MissingDataException;
use App\Trip\Exceptions\ExceptionCodes;
use App\Trip\Places\Place;

class PlaceFactoryTest extends TestCase
{
    public function placeFactoryProvider(): array
    {
        return [
            'placeFactory' => [new PlaceFactory()]
        ];
    }

    /** @dataProvider placeFactoryProvider */
    public function testPlaceFactory(PlaceFactory $placeFactory): void
    {
        $this->assertInstanceOf(PlaceFactory::class, $placeFactory);
    }

    /** @dataProvider placeFactoryProvider */
    public function testGetPlaceMissingData(PlaceFactory $placeFactory): void
    {
        $this->expectException(MissingDataException::class);
        $this->expectExceptionCode(ExceptionCodes::PLACE_MISSING_DATA);
        $placeFactory->getPlace([]);
    }

    /** @dataProvider placeFactoryProvider */
    public function testGetPlace(PlaceFactory $placeFactory): void
    {
        $place = $placeFactory->getPlace(['name' => 'Berlin', 'id' => '1']);
        $this->assertInstanceOf(Place::class, $place);
    }
}
