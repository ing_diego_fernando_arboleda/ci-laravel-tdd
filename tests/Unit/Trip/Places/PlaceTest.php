<?php

namespace Tests\Unit\Trip\Places;

use PHPUnit\Framework\TestCase;
use App\Trip\Places\Place;

class PlaceTest extends TestCase
{
    private const PLACE_NAME = 'Berlin';
    private const PLACE_ID = 1;

    public function placeDataprovider(): array
    {
        return [
            'placeDataprovider' => [new Place(self::PLACE_NAME, self::PLACE_ID)]
        ];
    }

    /** @dataProvider placeDataprovider */
    public function testPlace(Place $place): void
    {
        $this->assertInstanceOf(Place::class, $place);
    }

    /** @dataProvider placeDataprovider */
    public function testGetName(Place $place): void
    {
        $place->setName(self::PLACE_NAME);
        $this->assertEquals(self::PLACE_NAME, $place->getName());
    }

    /** @dataProvider placeDataprovider */
    public function testGetId(Place $place): void
    {
        $place->setId(self::PLACE_ID);
        $this->assertEquals(self::PLACE_ID, $place->getId());
    }
}
