<?php

namespace Tests\Unit\Trip\Exceptions;

use PHPUnit\Framework\TestCase;
use App\Trip\Exceptions\ApiException;

class ApiExceptionTest extends TestCase
{
    public function testCard(): void
    {
        $exception = new ApiException('a exception');
        $this->assertTrue(is_numeric(stripos($exception->__toString(), 'ApiException')));
    }
}
