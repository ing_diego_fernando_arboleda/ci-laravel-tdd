<?php

namespace Tests\Feature;

class AuthApiUnitTest extends ApiUnit
{
    public function testRegisterUser()
    {
        $response = $this->post(
            route('auth.register'),
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345',
                'name' => 'TestUser 1'
            ]
        );
        $this->assertRestResponse($response, 'REGISTER_OK');
    }

    public function testRegisterUserExists()
    {
        $this->seed();

        $response = $this->post(
            route('auth.register'),
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345',
                'name' => 'TestUser 1'
            ]
        );

        $this->assertRestResponse($response, 'USER_EXISTS');
    }

    public function testLogin()
    {
        $this->seed();

        $response = $this->post(
            route('auth.login'),
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );

        $this->assertRestResponse($response, 'LOGIN_OK');
    }

    public function testInvalidLogin()
    {
        $this->seed();

        $response = $this->post(
            route('auth.login'),
            [
                'email' => 'test_user@gmail.com',
                'password' => 'badPassword'
            ]
        );

        $this->assertRestResponse($response, 'USER_NOT_EXISTS');
    }

    public function testInvalidUser()
    {
        $response = $this->post(
            route('auth.login'),
            [
                'email' => 'badUser@gmail.com',
                'password' => 'badPassword'
            ]
        );

        $this->assertRestResponse($response, 'USER_NOT_EXISTS');
    }
}
