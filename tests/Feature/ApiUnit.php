<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Helpers\ResponseHelper;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\TestResponse;

class ApiUnit extends TestCase
{
    use DatabaseMigrations;

    protected function assertRestResponse(TestResponse $response, string $keyResponse): void
    {
        $response->dump();
        $response->assertStatus(
            ResponseHelper::getResponseAsArray(
                $keyResponse
            )[ResponseHelper::KEY_HTTP_CODE]
        );
        $response->assertJson(
            ResponseHelper::getResponseAsArray(
                $keyResponse
            )[ResponseHelper::KEY_RESPONSE]
        );
    }
}
