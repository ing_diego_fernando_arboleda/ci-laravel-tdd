<?php

namespace App\Jwt;

use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtGenerator
{
    public static function generate(User $user, int $expiration = null): string
    {
        $payload = [
            'iss' => env('JWT_TOKEN_ISS', 'lumen-jwt'),
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + ($expiration ?? env('JWT_TOKEN_EXPIRATION', 3600))
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }
}
