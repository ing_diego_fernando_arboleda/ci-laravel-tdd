<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Models\User;
use App\Helpers\ResponseHelper;
use Illuminate\Http\JsonResponse;
use App\Jwt\JwtGenerator;

class AuthController extends Controller
{
    public function login(AuthLoginRequest $request, User $user): JsonResponse
    {
        $postFields = $request->validated();

        $user = User::where('email', $postFields['email'])->first();

        if (!$user) {
            return ResponseHelper::getErrorResponse('USER_NOT_EXISTS');
        }

        if (!password_verify($request->input('password'), $user->password)) {
            //Revealing if the user exists is a security issue.
            return ResponseHelper::getErrorResponse('USER_NOT_EXISTS');
        }

        return $this->successResponse($user, 'LOGIN_OK');
    }

    public function register(AuthRegisterRequest $request, User $user): JsonResponse
    {
        $postFields = $request->validated();

        $userExists = User::where('email', $postFields['email'])->first();

        if ($userExists) {
            return ResponseHelper::getErrorResponse('USER_EXISTS');
        }

        $postFields['password'] = password_hash($postFields['password'], PASSWORD_DEFAULT);

        return $this->successResponse($user->create($postFields), 'REGISTER_OK');
    }

    private function successResponse(User $user, string $key): JsonResponse
    {
        return ResponseHelper::getResponse(
            [
                'token' => JwtGenerator::generate($user)
            ],
            $key
        );
    }
}
