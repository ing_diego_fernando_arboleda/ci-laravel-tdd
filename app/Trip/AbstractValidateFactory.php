<?php

namespace App\Trip;

use App\Trip\Transportation\AbstractTransport;
use App\Trip\ValidableFactoryInterface;

abstract class AbstractValidateFactory
{
    abstract protected function validate(array $transportInput): void;
}
