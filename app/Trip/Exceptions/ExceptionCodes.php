<?php

namespace App\Trip\Exceptions;

class ExceptionCodes
{
    public const CARD_INVALID_DATA = 100;
    public const TRANSPORT_MISSING_DATA = 200;
    public const TRANSPORT_INVALID_TYPE = 201;
    public const PLACE_MISSING_DATA = 300;
}
