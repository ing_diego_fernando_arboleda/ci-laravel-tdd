<?php

namespace App\Trip\Exceptions\Transportation\Factories;

use App\Trip\Exceptions\ExceptionCodes;
use App\Trip\Exceptions\ApiException;

class InvalidTransportException extends ApiException
{
    public function __construct(
        string $message,
        int $code = ExceptionCodes::TRANSPORT_INVALID_TYPE,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
