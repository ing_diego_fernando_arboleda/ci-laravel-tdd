<?php

namespace App\Trip\Exceptions\Transportation\Factories;

use App\Trip\Exceptions\ExceptionCodes;
use App\Trip\Exceptions\ApiException;

class MissingDataException extends ApiException
{
    public function __construct(
        string $message,
        int $code = ExceptionCodes::TRANSPORT_MISSING_DATA,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
