<?php

namespace App\Trip\Exceptions\Cards\Factories;

use App\Trip\Exceptions\ExceptionCodes;
use App\Trip\Exceptions\ApiException;

class InvalidCardException extends ApiException
{
    public function __construct(
        string $message,
        int $code = ExceptionCodes::CARD_INVALID_DATA,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
