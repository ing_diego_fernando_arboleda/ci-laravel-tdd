<?php

namespace App\Trip\Exceptions;

class ApiException extends \RunTimeException
{
    public function __toString(): string
    {
        $message = __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        $message .= ' at line: ' . $this->getLine() . ' on file: ' . $this->getFile();
        return $message;
    }
}
