<?php

namespace App\Trip\Exceptions\Places\Factories;

use App\Trip\Exceptions\ExceptionCodes;
use App\Trip\Exceptions\ApiException;

class MissingDataException extends ApiException
{
    public function __construct(
        string $message,
        int $code = ExceptionCodes::PLACE_MISSING_DATA,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
