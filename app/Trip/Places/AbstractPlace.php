<?php

namespace App\Trip\Places;

abstract class AbstractPlace implements PlaceInterface
{
    private string $name;
    private int $identifier;

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $identifier): void
    {
        $this->identifier = $identifier;
    }

    public function getId(): int
    {
        return $this->identifier;
    }
}
