<?php

namespace App\Trip\Places;

interface PlaceInterface
{
    public function setName(string $name): void;

    public function getName(): string;

    public function setId(int $identifier);

    public function getId(): int;
}
