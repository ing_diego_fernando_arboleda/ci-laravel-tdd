<?php

namespace App\Trip\Places;

class Place extends AbstractPlace
{
    public function __construct($name, $identifier)
    {
        $this->setName($name);
        $this->setId($identifier);
    }
}
