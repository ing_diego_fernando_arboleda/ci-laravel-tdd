<?php

namespace App\Trip\Places\Factories;

use App\Trip\Places\AbstractPlace;

interface PlaceFactoryInterface
{
    public function getPlace(array $placeData): AbstractPlace;
}
