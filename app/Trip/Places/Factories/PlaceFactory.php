<?php

namespace App\Trip\Places\Factories;

use App\Trip\Exceptions\Places\Factories\MissingDataException;
use App\Trip\AbstractValidateFactory;
use App\Trip\Places\AbstractPlace;
use App\Trip\Places\Place;

class PlaceFactory extends AbstractValidateFactory implements PlaceFactoryInterface
{
    protected function validate($transportInput): void
    {
        if (
            !isset($transportInput['name']) ||
            !isset($transportInput['id'])
        ) {
            throw new MissingDataException(
                'missing data for transport'
            );
        }
    }

    public function generate(array $placeData): AbstractPlace
    {
        $place = new Place($placeData['name'], $placeData['id']);
        return $place;
    }

    public function getPlace(array $placeData): AbstractPlace
    {
        $this->validate($placeData);
        return $this->generate($placeData);
    }
}
