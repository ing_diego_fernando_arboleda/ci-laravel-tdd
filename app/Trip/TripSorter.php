<?php

namespace App\Trip;

use App\Trip\Cards\Factories\CardFactoryInterface;

class TripSorter
{
    private CardFactoryInterface $cardFactory;

    public function __construct(CardFactoryInterface $cardFactory)
    {
        $this->cardFactory = $cardFactory;
    }

    /**
     *
     * @param  array  $unsortCards a matrix of array with format
     *  [
     *       'transport' => [
     *           'type' => 'bus',
     *           'seat' => '',
     *           'name' => 'airport'
     *       ],
     *       'origin' => [
     *           'name' => 'Madrid',
     *           'id' => '15'
     *       ],
     *       'destination' => [
     *           'name' => 'Barcelona',
     *           'id' => '1'
     *       ],
     *       'gate' => '',
     *       'baggageMessage' => ''
     *   ]
     */
    public function sort(array $unsortCards): array
    {
        //Build and validate data.
        $cards = [];
        foreach ($unsortCards as $tmpCard) {
            $cards[] = $this->cardFactory->buildCard($tmpCard);
        }

        //prepare data: index origin and destination.
        $indexedAirports = $this->prepareData($cards);

        //process data: sort cards.
        $merged = [];
        $merged[] = $cards[0];
        $itinerary = $this->recursiveSort(
            $indexedAirports['origin'],
            $indexedAirports['destination'],
            $merged
        );
        return $itinerary;
    }

    public function prepareData(array $listUnsortCards): array
    {
        $origin = [];
        $destination = [];
        foreach ($listUnsortCards as $card) {
            $origin[$card->getOrigin()->getId()] = $card;
            $destination[$card->getDestination()->getId()] = $card;
        }

        return ['origin' => $origin, 'destination' => $destination];
    }

    public function recursiveSort(
        array $origin,
        array $destination,
        array $merged
    ): array {
        $isMerged = false;
        if (isset($destination[$merged[0]->getOrigin()->getId()])) {
            array_unshift(
                $merged,
                $destination[$merged[0]->getOrigin()->getId()]
            );
            $isMerged = true;
        }

        if (isset($origin[$merged[count($merged) - 1]->getDestination()->getId()])) {
            array_push(
                $merged,
                $origin[$merged[count($merged) - 1]->getDestination()->getId()]
            );
            $isMerged = true;
        }

        if ($isMerged) {
            return $this->recursiveSort($origin, $destination, $merged);
        } else {
            $merged[count($merged) - 1]->appendToBaggageMessage();
            return $merged;
        }
    }
}
