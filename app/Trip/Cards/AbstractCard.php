<?php

namespace App\Trip\Cards;

use App\Trip\Places\AbstractPlace;
use App\Trip\Transportation\AbstractTransport;

abstract class AbstractCard implements CardInterface
{
    public const WELCOME_MESSAGE = 'You have arrived at your final destination.';

    public const SEPARATOR = "\n";

    protected AbstractPlace $origin;

    protected AbstractPlace $destination;

    protected AbstractTransport $transport;

    protected string $gate;

    protected string $baggageMessage;

    public function setOrigin(AbstractPlace $origin): void
    {
        $this->origin = $origin;
    }

    public function getOrigin(): AbstractPlace
    {
        return $this->origin;
    }

    public function setDestination(AbstractPlace $destination): void
    {
        $this->destination = $destination;
    }

    public function getDestination(): AbstractPlace
    {
        return $this->destination;
    }

    public function setTransport(AbstractTransport $transport): void
    {
        $this->transport = $transport;
    }

    public function getTransport(): AbstractTransport
    {
        return $this->transport;
    }

    public function setGate(string $gate): void
    {
        $this->gate = $gate;
    }

    public function getGate(): string
    {
        return $this->gate;
    }

    public function setBaggageMessage(string $baggageMessage): void
    {
        $this->baggageMessage = $baggageMessage;
    }

    public function getBaggageMessage(): string
    {
        return $this->baggageMessage;
    }

    public function appendToBaggageMessage(
        string $baggageMessage = self::WELCOME_MESSAGE
    ): void {
        $this->baggageMessage .= self::SEPARATOR . $baggageMessage;
    }
}
