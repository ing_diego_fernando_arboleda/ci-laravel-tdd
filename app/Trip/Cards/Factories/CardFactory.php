<?php

namespace App\Trip\Cards\Factories;

use App\Trip\Places\AbstractPlace;
use App\Trip\Transportation\AbstractTransport;
use App\Trip\Exceptions\Cards\Factories\InvalidCardException;
use App\Trip\Transportation\Factories\TransportFactoryInterface;
use App\Trip\Places\Factories\PlaceFactoryInterface;
use App\Trip\AbstractValidateFactory;
use App\Trip\Cards\AbstractCard;
use App\Trip\Cards\Card;

class CardFactory extends AbstractValidateFactory implements CardFactoryInterface
{
    private TransportFactoryInterface $transportFactory;

    private PlaceFactoryInterface $placeFactory;

    protected function validate($card): void
    {
        if (
            !is_array($card) ||
            !isset($card['transport']) ||
            !isset($card['origin']) ||
            !isset($card['destination']) ||
            !isset($card['gate']) ||
            !isset($card['baggageMessage'])
        ) {
            throw new InvalidCardException('invalid card data');
        }
    }

    public function __construct(
        TransportFactoryInterface $transportFactory,
        PlaceFactoryInterface $placeFactory
    ) {
        $this->transportFactory = $transportFactory;
        $this->placeFactory = $placeFactory;
    }

    public function getCard(
        string $gate,
        string $baggageMessage,
        AbstractPlace $origin,
        AbstractPlace $destination,
        AbstractTransport $transport
    ): AbstractCard {
        $card = new Card();
        $card->setGate($gate);
        $card->setBaggageMessage($baggageMessage);
        $card->setOrigin($origin);
        $card->setDestination($destination);
        $card->setTransport($transport);
        return $card;
    }

    public function buildCard(array $card): AbstractCard
    {
        $this->validate($card);
        return $this->getCard(
            $card['gate'],
            $card['baggageMessage'],
            $this->placeFactory->getPlace($card['origin']),
            $this->placeFactory->getPlace($card['destination']),
            $this->transportFactory->getTransport($card['transport'])
        );
    }
}
