<?php

namespace App\Trip\Cards\Factories;

use App\Trip\Places\AbstractPlace;
use App\Trip\Transportation\AbstractTransport;
use App\Trip\Cards\AbstractCard;

interface CardFactoryInterface
{
    public function getCard(
        string $gate,
        string $baggageMessage,
        AbstractPlace $origin,
        AbstractPlace $destination,
        AbstractTransport $transport
    ): AbstractCard;

    public function buildCard(array $card): AbstractCard;
}
