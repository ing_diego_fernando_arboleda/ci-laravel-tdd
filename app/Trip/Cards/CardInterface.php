<?php

namespace App\Trip\Cards;

use App\Trip\Places\AbstractPlace;
use App\Trip\Transportation\AbstractTransport;

interface CardInterface
{
    public function setOrigin(AbstractPlace $origin): void;

    public function getOrigin(): AbstractPlace;

    public function setDestination(AbstractPlace $destination): void;

    public function getDestination(): AbstractPlace;

    public function setTransport(AbstractTransport $transport): void;

    public function getTransport(): AbstractTransport;

    public function setGate(string $gate): void;

    public function getGate(): string;

    public function setBaggageMessage(string $baggageMessage): void;

    public function getBaggageMessage(): string;

    public function getItinerary(): string;

    public function appendToBaggageMessage(string $baggageMessage): void;
}
