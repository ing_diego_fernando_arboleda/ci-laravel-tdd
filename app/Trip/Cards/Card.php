<?php

namespace App\Trip\Cards;

class Card extends AbstractCard
{
    public function getItinerary(): string
    {
        $msg = $this->getTransport()->getMessage();

        $msg = str_replace(
            ['{@ORIGIN}','{@DESTINATION}','{@GATE}'],
            [   $this->getOrigin()->getName(),
                $this->getDestination()->getName(),
                $this->getGate()
            ],
            $msg
        );

        if ($this->getBaggageMessage()) {
            $msg .= '. ' . $this->getBaggageMessage();
        }

        return $msg;
    }
}
