<?php

namespace App\Trip\Transportation;

use App\Trip\Transportation\AbstractTransport;

class Flight extends AbstractTransport
{
    public function getMessage(): string
    {
        $msg = 'From {@ORIGIN}, take flight ' . $this->getName();
        $msg .= ' to {@DESTINATION}. Gate {@GATE}';
        $seat = $this->getSeat();
        if ($seat) {
            $msg .= ',seat ' . $seat;
        } else {
            $msg .= ',no seat assignment';
        }
        return $msg;
    }
}
