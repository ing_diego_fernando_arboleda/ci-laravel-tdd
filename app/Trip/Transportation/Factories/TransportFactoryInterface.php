<?php

namespace App\Trip\Transportation\Factories;

use App\Trip\Transportation\AbstractTransport;

interface TransportFactoryInterface
{
    public function getTransport(array $transportInput): AbstractTransport;
}
