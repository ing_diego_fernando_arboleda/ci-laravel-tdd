<?php

namespace App\Trip\Transportation\Factories;

use App\Trip\Exceptions\Transportation\Factories\InvalidTransportException;
use App\Trip\Exceptions\Transportation\Factories\MissingDataException;
use App\Trip\Transportation\AbstractTransport;
use App\Trip\Transportation\Bus;
use App\Trip\Transportation\Train;
use App\Trip\Transportation\Flight;
use App\Trip\Validator\TransportFactoryValidator;

class TransportFactory extends AbstractTransportFactory
{
    protected function validate($transportInput): void
    {
        if (
            !isset($transportInput['type']) ||
            !isset($transportInput['seat']) ||
            !isset($transportInput['name'])
        ) {
            throw new MissingDataException(
                'missing data for transport'
            );
        }
    }

    protected function generate($transportInput): AbstractTransport
    {
        switch ($transportInput['type']) {
            case 'train':
                $transport = new Train();
                break;
            case 'bus':
                $transport = new Bus();
                break;
            case 'flight':
                $transport = new Flight();
                break;
            default:
                throw new InvalidTransportException(
                    'transport type ' . $transportInput['type'] . ' not suported'
                );
        }

        $transport->setSeat($transportInput['seat']);
        $transport->setName($transportInput['name']);

        return $transport;
    }
}
