<?php

namespace App\Trip\Transportation\Factories;

use App\Trip\Transportation\AbstractTransport;
use App\Trip\AbstractValidateFactory;

abstract class AbstractTransportFactory extends AbstractValidateFactory implements TransportFactoryInterface
{
    abstract protected function generate(array $transportInput): AbstractTransport;

    public function getTransport(array $transportInput): AbstractTransport
    {
        $this->validate($transportInput);
        return $this->generate($transportInput);
    }
}
