<?php

namespace App\Trip\Transportation;

use App\Trip\Transportation\AbstractTransport;

class Train extends AbstractTransport
{
    public function getMessage(): string
    {
        $msg = 'Take train ' . $this->getName() . ' from {@ORIGIN} to {@DESTINATION}. ';
        $seat = $this->getSeat();
        if ($seat) {
            $msg .= 'Sit in seat ' . $seat;
        } else {
            $msg .= 'No seat assignment';
        }
        return $msg;
    }
}
