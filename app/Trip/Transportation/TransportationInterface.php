<?php

namespace App\Trip\Transportation;

interface TransportationInterface
{
    public function setName(string $name): void;

    public function getName(): string;

    public function setSeat(string $seat): void;

    public function getSeat(): string;
}
