<?php

namespace App\Trip\Transportation;

use App\Trip\Transportation\TransportationInterface;

abstract class AbstractTransport implements TransportationInterface
{
    private string $name;

    private string $seat;

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setSeat(string $seat): void
    {
        $this->seat = $seat;
    }

    public function getSeat(): string
    {
        return $this->seat;
    }

    abstract public function getMessage(): string;
}
