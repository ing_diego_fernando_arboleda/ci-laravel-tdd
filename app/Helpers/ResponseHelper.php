<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;

class ResponseHelper
{
    private const KEY_CODE = 'code';
    private const KEY_MESSAGE = 'message';
    public const KEY_HTTP_CODE = 'http_code';
    public const KEY_RESPONSE = 'response';

    public static function responses(): array
    {
        return [
            /*Auth*/
            'NOT_ALLOWED' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.notallowed'),
                    self::KEY_CODE => 1001
                ],
                self::KEY_HTTP_CODE => 401,
            ],
            'TOKEN_NOT_PROVIDED' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.missingtoken'),
                    self::KEY_CODE => 1002
                ],
                self::KEY_HTTP_CODE => 401,
            ],
            'TOKEN_EXPIRED' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.epiredtoken'),
                    self::KEY_CODE => 1003
                ],
                self::KEY_HTTP_CODE => 400,
            ],
            'TOKEN_INVALID' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.notallowed'),
                    self::KEY_CODE => 1004
                ],
                self::KEY_HTTP_CODE => 400,
            ],

            /*User*/
            'LOGIN_OK' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.loginok'),
                    self::KEY_CODE => 2000
                ],
                self::KEY_HTTP_CODE => 200,
            ],
            'REGISTER_OK' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.registerok'),
                    self::KEY_CODE => 2001
                ],
                self::KEY_HTTP_CODE => 201,
            ],
            'USER_NOT_EXISTS' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.failed'),
                    self::KEY_CODE => 2002
                ],
                self::KEY_HTTP_CODE => 400,
            ],
            'USER_EXISTS' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.exists'),
                    self::KEY_CODE => 2003
                ],
                self::KEY_HTTP_CODE => 500,
            ],
            'USER_DISABLED' => [
                self::KEY_RESPONSE => [
                    self::KEY_MESSAGE => __('auth.disabled'),
                    self::KEY_CODE => 2004
                ],
                self::KEY_HTTP_CODE => 400,
            ],
        ];
    }

    public static function getErrorResponse(string $key): JsonResponse
    {
        $response = self::getResponseAsArray($key);
        return response()->json(
            $response[self::KEY_RESPONSE],
            $response[self::KEY_HTTP_CODE]
        );
    }

    public static function getResponse(array $data, string $key): JsonResponse
    {
        $response = self::getResponseAsArray($key);
        return response()->json(
            array_merge($data, $response[self::KEY_RESPONSE]),
            $response[self::KEY_HTTP_CODE]
        );
    }

    public static function getResponseAsArray(string $key): array
    {
        return self::responses()[$key];
    }
}
